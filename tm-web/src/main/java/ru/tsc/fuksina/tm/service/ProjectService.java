package ru.tsc.fuksina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    public Project save(@NotNull final String name) {
        final Project project = new Project(name);
        return projectRepository.save(project);
    }

    public Project save(@NotNull final Project project) {
        return projectRepository.save(project);
    }

    public void removeById(@NotNull final String id) {
        projectRepository.deleteById(id);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    public Project findById(@NotNull final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    public void remove(@NotNull final Project project) {
        projectRepository.delete(project);
    }

    public void remove(@NotNull final List<Project> projects) {
        projects.forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return projectRepository.existsById(id);
    }

    public void clear() {
        projectRepository.deleteAll();
    }

    public long count() {
        return projectRepository.count();
    }

}
