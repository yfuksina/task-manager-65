package ru.tsc.fuksina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.fuksina.tm.model.Task;
import ru.tsc.fuksina.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    public Task save(@NotNull final String name) {
        final Task task = new Task(name);
        return taskRepository.save(task);
    }

    public Task save(@NotNull final Task task) {
        return taskRepository.save(task);
    }

    public void removeById(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @NotNull
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    public Task findById(@NotNull final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    public void remove(@NotNull final Task task) {
        taskRepository.delete(task);
    }

    public void remove(@NotNull final List<Task> tasks) {
        tasks.forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return taskRepository.existsById(id);
    }

    public void clear() {
        taskRepository.deleteAll();
    }

    public long count() {
        return taskRepository.count();
    }

}
