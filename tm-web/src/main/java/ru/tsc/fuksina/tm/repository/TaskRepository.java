package ru.tsc.fuksina.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

}
