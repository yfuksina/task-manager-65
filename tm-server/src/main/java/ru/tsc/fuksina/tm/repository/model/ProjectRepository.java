package ru.tsc.fuksina.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.model.Project;

@Repository
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

}
