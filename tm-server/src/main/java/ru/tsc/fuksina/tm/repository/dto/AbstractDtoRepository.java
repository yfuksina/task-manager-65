package ru.tsc.fuksina.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.fuksina.tm.dto.model.AbstractModelDto;

@NoRepositoryBean
public interface AbstractDtoRepository<M extends AbstractModelDto> extends JpaRepository<M, String> {

}

