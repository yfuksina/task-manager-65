package ru.tsc.fuksina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created_dt")
    private Date created = new Date();

    @Nullable
    @Column(name = "start_dt")
    private Date dateStart;

    @Nullable
    @Column(name = "end_dt")
    private Date dateEnd;

    public AbstractUserOwnedModel(
            final @NotNull String name,
            final @NotNull String description,
            final @NotNull Status status,
            final @Nullable Date dateStart
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateStart = dateStart;
    }


}
