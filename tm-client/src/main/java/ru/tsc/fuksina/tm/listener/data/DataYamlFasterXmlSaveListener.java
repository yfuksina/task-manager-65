package ru.tsc.fuksina.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.DataYamlFasterXmlSaveRequest;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.event.ConsoleEvent;

@Component
public final class DataYamlFasterXmlSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-yaml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in yaml file";

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlFasterXmlSaveListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SAVE DATA IN YAML FILE]");
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlFasterXmlSaveRequest(getToken()));
    }

}
